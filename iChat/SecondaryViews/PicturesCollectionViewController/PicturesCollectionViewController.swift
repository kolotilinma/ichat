//
//  PicturesCollectionViewController.swift
//  iChat
//
//  Created by Михаил on 04.02.2020.
//  Copyright © 2020 MikhailKolotilin. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import SDWebImage

class PicturesCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, SKPhotoBrowserDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let itemsPerRow: CGFloat = 3
    private let minimumItemSpacing: CGFloat = 2
    let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 0.0, right: 5.0)
    
    var allImageLinks: [String] = []
    var allImages: [UIImage] = []
    var images = [SKPhotoProtocol]()
    
    // Есть баг!!! при не полной загрузки истории пепеписки, collectionView падает, связано с тем что не совпадает количество картинок в памяти
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "All Pictures"
        // Static setup
        SKPhotoBrowserOptions.displayAction = true
        SKPhotoBrowserOptions.displayStatusbar = true
        SKPhotoBrowserOptions.displayCounterLabel = true
        SKPhotoBrowserOptions.displayBackAndForwardButton = true
        
        if allImageLinks.count > 0 {
            downloadImages()
        }
        
        setupTestData()
        setupCollectionView()
        collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

 // MARK: - UICollectionViewDataSource
extension PicturesCollectionViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    @objc(collectionView:cellForItemAtIndexPath:) func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "picturesCollectionViewCell", for: indexPath) as? PicturesCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.generateCell(image: allImages[(indexPath as NSIndexPath).row])
        cell.imageView.contentMode = .scaleAspectFill
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension PicturesCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    @objc(collectionView:didSelectItemAtIndexPath:) func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let browser = SKPhotoBrowser(photos: images, initialPageIndex: indexPath.row)
        browser.delegate = self

        present(browser, animated: true, completion: {})
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left + sectionInsets.right + minimumItemSpacing * (itemsPerRow - 1)
        let availableWidth = collectionView.bounds.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumItemSpacing
    }
    
}



// MARK: - SKPhotoBrowserDelegate

extension PicturesCollectionViewController {
    func didShowPhotoAtIndex(_ index: Int) {
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    
    func willDismissAtPageIndex(_ index: Int) {
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    
    func willShowActionSheet(_ photoIndex: Int) {
        // do some handle if you need
    }
    
    func didDismissAtPageIndex(_ index: Int) {
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = false
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
        // handle dismissing custom actions
    }
    
    func removePhoto(_ browser: SKPhotoBrowser, index: Int, reload: @escaping (() -> Void)) {
        reload()
    }

    func viewForPhoto(_ browser: SKPhotoBrowser, index: Int) -> UIView? {
        return collectionView.cellForItem(at: IndexPath(item: index, section: 0))
    }
    
    func captionViewForPhotoAtIndex(index: Int) -> SKCaptionView? {
        return nil
    }
}

// MARK: - private

private extension PicturesCollectionViewController {
    func setupTestData() {
        images = createWebPhotos()
    }
    
    func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<allImageLinks.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImage(allImages[i])
            return photo
        }
    }
    
    //MARK: DownloadImages
    
    func downloadImages() {
        for imageLink in allImageLinks {
            downloadImage(imageUrl: imageLink) { (image) in
                if image != nil {
                    self.allImages.append(image!)
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
}

class PicturesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.image = nil
        layer.cornerRadius = 2.0
        layer.masksToBounds = true
    }
    
    override func prepareForReuse() {
        imageView.image = nil
    }
    
    func generateCell(image: UIImage) {
        self.imageView.image = image
    }
}


