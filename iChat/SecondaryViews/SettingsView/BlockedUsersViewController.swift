//
//  BlockedUsersViewController.swift
//  iChat
//
//  Created by Михаил on 06.02.2020.
//  Copyright © 2020 MikhailKolotilin. All rights reserved.
//

import UIKit
import JGProgressHUD
import NVActivityIndicatorView

class BlockedUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UserTableViewCellDelegate {

    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var blockedUserArray: [FUser] = []
    
    let hud = JGProgressHUD(style: .dark)
    var activityIdicator: NVActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        
        activityIdicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.width / 2 - 30, y: self.view.frame.height / 2 - 30, width: 60.0, height: 60.0), type: .ballSpinFadeLoader, color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), padding: nil)
        
        tableView.tableFooterView = UIView()
        loadUsers()
    }
    
    // MARK: - TableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        notificationLabel.isHidden = blockedUserArray.count != 0
        return blockedUserArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserTableViewCell
        cell.delegate = self
        cell.generateCellWith(fUser: blockedUserArray[indexPath.row], indexPath: indexPath)
        
        return cell
    }
    
    // MARK: - TableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Unblock"
    }
    
    // bugfix  при нажатии на аватар при переходе на
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard var tempBlockedUsers = FUser.currentUser()?.blockedUsers else { return }
        
        let userIdToUnBlock = blockedUserArray[indexPath.row].objectId
        tempBlockedUsers.remove(at: tempBlockedUsers.firstIndex(of: userIdToUnBlock)!)
        blockedUserArray.remove(at: indexPath.row)
        
        updateCurrentUserInFirestore(withValues: [kBLOCKEDUSERID : tempBlockedUsers]) { (error) in
            if error != nil {
                self.hud.textLabel.text = error!.localizedDescription
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
            }
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - LoadBlockedd Users
    func loadUsers() {
        if FUser.currentUser()!.blockedUsers.count > 0 {
            showLoadingIdicator()
            getUsersFromFirestore(withIds: FUser.currentUser()!.blockedUsers) { (allBlockedUsers) in
                self.hideLoadingIdicator()
                self.blockedUserArray = allBlockedUsers
                self.tableView.reloadData()
            }
        }
    }
    
    // bugfix  при нажатии на аватар при переходе на 
    // MARK: - UserTableViewCellDelegate
    func didTapAvatarImage(indexPath: IndexPath) {
        let profileVC: ProfileViewTableViewController = ProfileViewTableViewController.loadFromStoryboard()
        profileVC.user = blockedUserArray[indexPath.row]
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    //MARK: - Activity Indicator
    
    private func showLoadingIdicator() {
        if activityIdicator != nil {
            self.view.addSubview(activityIdicator!)
            activityIdicator!.startAnimating()
        }
    }

    private func hideLoadingIdicator() {
        if activityIdicator != nil {
            activityIdicator!.removeFromSuperview()
            activityIdicator!.stopAnimating()
        }
    }
    
}
