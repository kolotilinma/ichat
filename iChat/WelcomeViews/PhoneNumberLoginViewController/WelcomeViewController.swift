//
//  PhoneNumberLoginViewController.swift
//  iChat
//
//  Created by Михаил Колотилин on 13.02.2020.
//  Copyright © 2020 MikhailKolotilin. All rights reserved.
//

import UIKit
import FirebaseAuth
import JGProgressHUD
import NVActivityIndicatorView

class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var requestButtonOutlet: UIButton!
    @IBOutlet weak var registerButtonOutlet: UIButton!
    
    var phoneNumber: String!
    var verificationId: String?
    
    let hud = JGProgressHUD(style: .dark)
    var activityIdicator: NVActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIdicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.width / 2 - 30, y: self.view.frame.height / 2 - 30, width: 60.0, height: 60.0), type: .ballSpinFadeLoader, color: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), padding: nil)
        setupUIWithAnimation()
    }
    
    
    // MARK: - IBActions
    
    @IBAction func requestButtonPressed(_ sender: Any) {
        //register
        if verificationId != nil {
            registerUser()
            return
        }
        
        // request code
        if mobileNumberTextField.text != "" && countryCodeTextField.text != "" {
            let fullNumber = countryCodeTextField.text! + mobileNumberTextField.text!
            showLoadingIdicator()
            
            PhoneAuthProvider.provider().verifyPhoneNumber(fullNumber, uiDelegate: nil) { (_verificationId, error) in
                if error != nil {
                    self.hud.textLabel.text = error!.localizedDescription
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.show(in: self.view)
                    self.hud.dismiss(afterDelay: 2.0)
                    return
                }
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 1.0)
                self.verificationId = _verificationId
                self.updateUI()
            }
            
        } else {
            hud.textLabel.text = "Phone number is required!"
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
            hud.show(in: self.view)
            hud.dismiss(afterDelay: 2.0)
        }
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        dismissKeyboard()
        hud.textLabel.text = "Registering..."
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 2.0)
        
        if nameTextField.text != "" && surnameTextField.text != "" && countryTextField.text != "" && cityTextField.text != "" {
            self.endRegisterUser()
        } else {
            hud.textLabel.text = "All fields are required!"
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
            hud.show(in: self.view)
            hud.dismiss(afterDelay: 2.0)
        }
        
    }
    
    // MARK: - Helpers
    
    func setupUIWithAnimation() {
        countryCodeTextField.text = CountryCode().currentCode
        
        UIView.animate(withDuration: 0.6, animations: {
            self.titleLabel.alpha = 1
        })
        
        UIView.animate(withDuration: 1.2, animations: {
            self.countryCodeTextField.alpha = 1
            self.mobileNumberTextField.alpha = 1
        })
        
        UIView.animate(withDuration: 1.5, animations: {
            self.requestButtonOutlet.alpha = 1
        })
    }
    
    func updateUI() {
        requestButtonOutlet.setTitle("Submit", for: .normal)
        phoneNumber = countryCodeTextField.text! + mobileNumberTextField.text!
        countryCodeTextField.isEnabled = false
        countryCodeTextField.placeholder = countryCodeTextField.text
        countryCodeTextField.text = ""
        mobileNumberTextField.isEnabled = false
        mobileNumberTextField.placeholder = mobileNumberTextField.text
        mobileNumberTextField.text = ""
        
        UIView.animate(withDuration: 0.3, animations: {
            self.codeTextField.isHidden = false
            self.codeTextField.becomeFirstResponder()
        })
        
        UIView.transition(with: titleLabel, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.titleLabel.text = "Verify Pin"
        }, completion: nil)
        
        hideLoadingIdicator()
    }
    
    func finishUpdateUI() {
        hideLoadingIdicator()
        
        UIView.transition(with: titleLabel, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
            self?.titleLabel.text = "Enter your Name and City"
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.codeTextField.isHidden = true
            self.nameTextField.isHidden = false
            self.surnameTextField.isHidden = false
            self.countryTextField.isHidden = false
            self.cityTextField.isHidden = false
        })
        
        registerButtonOutlet.isHidden = false
        requestButtonOutlet.isHidden = true
    }
    
    func registerUser() {
        if codeTextField.text != "" && verificationId != nil {
            showLoadingIdicator()
            
            FUser.registerUserWith(phoneNumber: phoneNumber, countryCode: self.countryCodeTextField.placeholder!, verificationCode: codeTextField.text!, verificationId: verificationId) { (error, shouldLogin) in
                if error != nil {
                    self.hud.textLabel.text = error!.localizedDescription
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.show(in: self.view)
                    self.hud.dismiss(afterDelay: 2.0)
                    return
                }
                
                if shouldLogin {
                    // go to app
                    self.hud.dismiss(afterDelay: 1.0)
                    self.hideLoadingIdicator()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION),
                                                    object: nil, userInfo: [kUSERID : FUser.currentId()])
                    let mainView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "MainApplication") as! UITabBarController
                    mainView.modalPresentationStyle = .fullScreen
                    self.present(mainView, animated: true, completion: nil)
                } else {
                    self.finishUpdateUI()
                }
            }
        } else {
            hud.textLabel.text = "Please insert the code!"
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
            hud.show(in: self.view)
            hud.dismiss(afterDelay: 2.0)
        }
    }
    
    func endRegisterUser() {
        let fullName = nameTextField.text! + " " + surnameTextField.text!
        
        var tempDictionary : Dictionary = [kFIRSTNAME : nameTextField.text!,
                                           kLASTNAME : surnameTextField.text!,
                                           kFULLNAME : fullName,
                                           kCOUNTRY : countryTextField.text!,
                                           kCITY : cityTextField.text!,
                                           kCOUNTRYCODE : countryCodeTextField.placeholder!,
                                           kPHONE : mobileNumberTextField.placeholder!] as [String : Any]
        
        
        imageFromInitials(firstName: nameTextField.text!, lastName: surnameTextField.text!) { (avatarInitials) in
            let avatarIMG = avatarInitials.jpegData(compressionQuality: 0.7)
            let avatar = avatarIMG!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
            
            tempDictionary[kAVATAR] = avatar
            
            self.finishRegistration(withValues: tempDictionary)
        }
        
        
    }
    
    func finishRegistration(withValues: [String : Any]) {
        updateCurrentUserInFirestore(withValues: withValues) { (error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.hud.textLabel.text = error!.localizedDescription
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.show(in: self.view)
                    self.hud.dismiss(afterDelay: 2.0)
                }
                return
            }
            self.hideLoadingIdicator()
            self.goToApp()
        }
    }
    
    func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    func cleanTextFields() {
        nameTextField.text = ""
        surnameTextField.text = ""
        countryTextField.text = ""
        cityTextField.text = ""
        mobileNumberTextField.text = ""
    }

    func goToApp() {
        hideLoadingIdicator()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: USER_DID_LOGIN_NOTIFICATION), object: nil, userInfo: [kUSERID : FUser.currentId()])
        let mainView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "MainApplication") as! UITabBarController
        mainView.modalPresentationStyle = .fullScreen
        self.present(mainView, animated: true, completion: nil)
    }
    
    //MARK: - Activity Indicator
    
    private func showLoadingIdicator() {
        if activityIdicator != nil {
            self.view.addSubview(activityIdicator!)
            activityIdicator!.startAnimating()
        }
    }

    private func hideLoadingIdicator() {
        if activityIdicator != nil {
            activityIdicator!.removeFromSuperview()
            activityIdicator!.stopAnimating()
        }
    }
    
    
}
