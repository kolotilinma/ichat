//
//  GroupMemberCollectionViewCell.swift
//  iChat
//
//  Created by Михаил on 08.02.2020.
//  Copyright © 2020 MikhailKolotilin. All rights reserved.
//

import UIKit

protocol GroupMemberCollectionViewCellDelegate {
    func didClickDeleteButton(indexPath: IndexPath)
}

class GroupMemberCollectionViewCell: UICollectionViewCell {
    
    var indexPath: IndexPath!
    var delegate: GroupMemberCollectionViewCellDelegate?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    func genereteCell(user: FUser, indexPath: IndexPath) {
        self.indexPath = indexPath
        nameLabel.text = user.firstname
        if user.avatar != "" {
            imageFromData(pictureData: user.avatar) { (avatarImage) in
                guard let avatar = avatarImage else { return }
                self.avatarImageView.image = avatar.circleMasked
            }
        }
    }
    
    @IBAction func deleteButtonPresed(_ sender: Any) {
        delegate!.didClickDeleteButton(indexPath: indexPath)
    }
}
